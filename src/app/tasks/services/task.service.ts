import {Injectable} from '@angular/core';
import {CriticalityType, EstimationType, StateType, Task} from '../../essence/task';
import {Notification} from '../../essence/notification';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  private _taskList: Task[] = [];

  aliases = [
    { 'eng': 'name', 'rus': 'Название', 'visible': true },
    { 'eng': 'description', 'rus': 'Описание', 'visible': false  },
    { 'eng': 'checkList', 'rus': 'Чек лист для проверки задачи', 'visible': false  },
    { 'eng': 'documents', 'rus': 'Документы', 'visible': false  },
    { 'eng': 'estimation', 'rus': 'Оценка время исполнения', 'visible': false  },
    { 'eng': 'estimationType', 'rus': 'Тип оценки времени', 'visible': false  },
    { 'eng': 'dedline', 'rus': 'Дедлайн', 'visible': true  } ,
    { 'eng': 'criticality', 'rus': 'Критичность', 'visible': true  },
    { 'eng': 'tags', 'rus': 'Теги', 'visible': false  },
    { 'eng': 'startTime', 'rus': 'Время начала', 'visible': false  },
    { 'eng': 'endTime', 'rus': 'Время завершения', 'visible': false  },
    { 'eng': 'state', 'rus': 'Статус', 'visible': true  },
    { 'eng': 'notifications', 'rus': 'Оповещения', 'visible': false },
    { 'eng': 'editing', 'rus': 'Редактирование', 'visible': true }
  ];
  currentCriticality: string;
  criticality: string[];

  columnsToDisplay() {
    const lst = [];
    this.aliases.forEach( (item) =>  { if (item.visible) { lst.push(item.eng); } });
    return lst;
  }
    constructor() {

      this.criticality = [''];

      for (let i = 0; i < 3; i++) {
        this.criticality[i] = CriticalityType[i].toString();
      }

      for (let i = 0; i < 12; ++i) {
        this.addTask(
          'task number ' + i,
          'desc.',
          ['abc', 'abc', 'abc'],
          ['doc', 'doc', 'doc'],
          1,
          EstimationType.DAYS,
          new Date(0, 0, 0),
          CriticalityType.NOTIMPORTANT,
          [],
          new Date(0, 0, 0),
          new Date(0, 0, 0),
          StateType.DONE,
          []
        );
      }
      for (let i = 0; i < 12; ++i) {
        this.taskList[i].toObject();
      }

    }

  addTask(
    name: string,
    description: string,
    checkList: string[],
    documents: string[],
    estimation: number,
    estimationType: EstimationType,
    dedline: Date,
    criticality: CriticalityType,
    tags: string[],
    startTime: Date,
    endTime: Date,
    state: StateType,
    notifications: Notification[]
  ) {
    const task = new Task();
    task.name = name;
    task.description = description;
    task.checkList = checkList;
    task.documents = documents;
    task.estimation = estimation;
    task.estimationType = estimationType;
    task.dedline = dedline;
    task.criticality = criticality;
    task.tags = tags;
    task.startTime = startTime;
    task.endTime = endTime;
    task.state = state;
    task.notifications = notifications;
    this._taskList.push(task);
  }

  removeTaskByName(name: string): Task[] {
    return this._taskList.filter((task) => {
      if (task.name === name) {
        return false;
      }
    });
  }

  get taskList(): Task[] {
    return this._taskList;
  }

}
