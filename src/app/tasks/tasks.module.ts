import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TaskTableComponent} from './task-table/task-table.component';
import {PopupEditTaskComponent} from './popup-edit-task/popup-edit-task.component';
import {DeadlineCalendarComponent} from './deadline-calendar/deadline-calendar.component';
import {
  MatButtonModule, MatDatepickerModule,
  MatDialogModule,
  MatFormFieldModule, MatIconModule,
  MatInputModule, MatNativeDateModule,
  MatOptionModule,
  MatPaginatorModule, MatRadioModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ButtonsComponent } from './task-table/buttons/buttons.component';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import {UploadFileComponent} from '../plugins/uploads/upload-file/upload-file.component';

@NgModule({
  imports: [
    MatFormFieldModule,
    MatSelectModule,
    MatOptionModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatDialogModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatButtonModule,
    MatDatepickerModule,
    MatRadioModule,
    MatNativeDateModule,
    MatMomentDateModule,
  ],

  declarations: [
    TaskTableComponent,
    PopupEditTaskComponent,
    DeadlineCalendarComponent,
    ButtonsComponent,
    UploadFileComponent
  ],

  exports: [
    TaskTableComponent,
    ButtonsComponent,
    UploadFileComponent
  ],
  entryComponents: [
    PopupEditTaskComponent,
  ]

})
export class TasksModule { }
