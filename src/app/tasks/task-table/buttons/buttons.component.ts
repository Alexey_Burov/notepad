import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-buttons',
  templateUrl: './buttons.component.html',
  styleUrls: ['./buttons.component.css']
})
export class ButtonsComponent implements OnInit {
  private _taskName: string;

  constructor() { }

  @Output() emitBegin = new EventEmitter<string>();
  @Output() emitEnd = new EventEmitter<string>();
  @Output() emitEditing = new EventEmitter<string>();
  @Output() emitRemove = new EventEmitter<string>();

  @Input('task')
  set taskName(value) {
    this._taskName = value;
  }


  ngOnInit() {
  }

  begin() {
    this.emitBegin.emit(this._taskName);
  }

  end() {
    this.emitEnd.emit(this._taskName);
  }

  editing() {
    this.emitEditing.emit(this._taskName);
  }

  remove() {
    this.emitRemove.emit(this._taskName);
  }


}
