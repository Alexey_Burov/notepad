import {Component, Inject, OnInit} from '@angular/core';
import {TaskService} from '../services/task.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Task} from '../../essence/task';
import {TaskData} from '../../essence/task';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-popup-edit-task',
  templateUrl: './popup-edit-task.component.html',
  styleUrls: ['./popup-edit-task.component.css']
})
export class PopupEditTaskComponent implements OnInit {

  task: Task;
  ngOnInit() {
  }

  constructor(
      public dialogRef: MatDialogRef<PopupEditTaskComponent>,
      @Inject(MAT_DIALOG_DATA) public name: string,
      public taskService: TaskService,
  ) {

   this.task = taskService.taskList.find( function (task) {
     return task.toObject()['name'] === name['taskName'];
    });
  }



  onNoClick(): void {
    this.dialogRef.close();
  }

  onSave(): void {
    console.log('S A V E', this.task);

    this.dialogRef.close();
  }
}
