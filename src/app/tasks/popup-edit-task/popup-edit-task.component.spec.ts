import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupEditTaskComponent } from './popup-edit-task.component';

describe('PopupEditTaskComponent', () => {
  let component: PopupEditTaskComponent;
  let fixture: ComponentFixture<PopupEditTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupEditTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupEditTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
