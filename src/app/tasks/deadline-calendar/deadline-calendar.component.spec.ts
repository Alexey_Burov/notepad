import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeadlineCalendarComponent } from './deadline-calendar.component';

describe('DeadlineCalendarComponent', () => {
  let component: DeadlineCalendarComponent;
  let fixture: ComponentFixture<DeadlineCalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeadlineCalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeadlineCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
