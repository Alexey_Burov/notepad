import { Component, OnInit } from '@angular/core';
import {TaskService} from '../services/task.service';

@Component({
  selector: 'app-deadline-calendar',
  templateUrl: './deadline-calendar.component.html',
  styleUrls: ['./deadline-calendar.component.css']
})
export class DeadlineCalendarComponent implements OnInit {

  constructor(
    private taskService: TaskService
  ) { }

  ngOnInit() {
  }

}
