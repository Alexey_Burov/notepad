import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupEditEventComponent } from './popup-edit-event.component';

describe('PopupEditEventComponent', () => {
  let component: PopupEditEventComponent;
  let fixture: ComponentFixture<PopupEditEventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupEditEventComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupEditEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
