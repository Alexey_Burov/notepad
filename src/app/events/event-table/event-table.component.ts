import { Component, OnInit } from '@angular/core';
import {EventService} from '../services/event.service';
import {PopupEditEventComponent} from '../popup-edit-event/popup-edit-event.component';
import {MatDialog} from '@angular/material';

@Component({
  selector: 'app-event-table',
  templateUrl: './event-table.component.html',
  styleUrls: ['./event-table.component.css']
})
export class EventTableComponent {

  // constructor(
  //   private taskService: EventService
  // ) { }
  //

  animal: string;
  name: string;

  constructor(public dialog: MatDialog) {}

  openDialog(): void {

    console.log(this);

    const dialogRef = this.dialog.open(PopupEditEventComponent, {
      width: '250px',
      data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.animal = result;
    });
  }
  //
  // ngOnInit() {
  // }
}

