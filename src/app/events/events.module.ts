import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventTableComponent } from './event-table/event-table.component';
import { EventCalendarComponent } from './event-calendar/event-calendar.component';
import { PopupEditEventComponent } from './popup-edit-event/popup-edit-event.component';
import {MatDialogModule, MatFormFieldModule, MatInputModule} from '@angular/material';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule
  ],
    exports: [
        EventTableComponent
    ],
    entryComponents: [
      PopupEditEventComponent
    ],
    declarations: [
        EventTableComponent,
        EventCalendarComponent,
        PopupEditEventComponent
    ]
})
export class EventsModule { }
