import { Injectable } from '@angular/core';
import {ParseOptions} from '../../essence/parse-options';

@Injectable({
  providedIn: 'root'
})
export class ParseOptionsService extends ParseOptions{

  constructor() {
    super();
  }
}
