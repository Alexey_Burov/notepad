export class ParseOptions {
  get url(): string {
    return this._url;
  }

  set url(value: string) {
    this._url = value;
  }

  get placeCssSelector(): string {
    return this._placeCssSelector;
  }

  set placeCssSelector(value: string) {
    this._placeCssSelector = value;
  }

  get startTimeCssSelector(): string {
    return this._startTimeCssSelector;
  }

  set startTimeCssSelector(value: string) {
    this._startTimeCssSelector = value;
  }

  get organizerCssSelector(): string {
    return this._organizerCssSelector;
  }

  set organizerCssSelector(value: string) {
    this._organizerCssSelector = value;
  }

  get algorithm(): string {
    return this._algorithm;
  }

  set algorithm(value: string) {
    this._algorithm = value;
  }

  private _url: string;
  private _placeCssSelector: string;
  private _startTimeCssSelector: string;
  private _organizerCssSelector: string;
  private _algorithm: string;

}
