import {Notification} from './notification';

export enum EstimationType {
  HOURS,
  DAYS,
  WEEKS,
  OF100
}

export enum StateType {
  NEW,
  IN_PROCESS,
  DONE
}

export enum CriticalityType {
  NOTIMPORTANT,
  IMPORTANT,
  VERYIMPORTANT
}

export interface TaskData {
  name: string;
  description: string;
  checkList: string[];
  documents: string[];
  estimation: number;
  estimationType: EstimationType;
  dedline: Date;
  criticality: CriticalityType;
  tags: string[];
  startTime: Date;
  endTime: Date;
  state: StateType;
  notifications: Notification[];
  editing: null;
  toObject();
}

export class Task implements TaskData {

  toObject() {
    return Object(this);
  }

  constructor() {
  }

  checkList: string[];
  criticality: CriticalityType;
  dedline: Date;
  description: string;
  documents: string[];
  editing: null;
  endTime: Date;
  estimation: number;
  estimationType: EstimationType;
  name: string;
  notifications: Notification[];
  startTime: Date;
  state: StateType;
  tags: string[];
}
