import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TextComponent } from './text/text.component';
import { CKEditorPluginComponent } from './ckeditor-plugin/ckeditor-plugin.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import {FormsModule} from '@angular/forms';
import { PaintingComponent } from './painting/painting.component';

@NgModule({
    imports: [
        CommonModule,
        CKEditorModule,
        FormsModule
    ],
  exports: [
    CKEditorPluginComponent,
    PaintingComponent
  ],
    declarations: [
      TextComponent,
      CKEditorPluginComponent,
      PaintingComponent
    ]
})
export class RecordsModule { }
