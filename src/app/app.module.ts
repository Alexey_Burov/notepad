import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {TasksModule} from './tasks/tasks.module';
import {EventsModule} from './events/events.module';
import {RecordsModule} from './records/records.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,


  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    EventsModule,
    RecordsModule,
    TasksModule,
  ],
  providers: [],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
