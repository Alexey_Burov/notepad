import { Injectable } from '@angular/core';
import {User} from '../essence/user';

@Injectable({
  providedIn: 'root'
})
export class UserService extends User {

  constructor() {
    super();
  }
}
