export enum NotificationType {
  SMS,
  TELEGRAM,
  EMAIL
}

export class NotificationService {
  get type(): NotificationType {
    return this._type;
  }

  set type(value: NotificationType) {
    this._type = value;
  }

  get Data(): object {
    return this._Data;
  }
  set Data(value: object) {
    this._Data = value;
  }

  get createDate():Date {
    return this._createDate;
  }

  set createDate(value:Date) {
    this._createDate = value;
  }

  get updateDate():Date {
    return this._updateDate;
  }

  set updateDate(value:Date) {
    this._updateDate = value;
  }

  private _type: NotificationType;
  private _Data: object;
  private _createDate: Date;
  private _updateDate: Date;
}
