import {NotificationService} from './notification-service';

export class User {
  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }

  get password(): string {
    return this._password;
  }

  set password(value: string) {
    this._password = value;
  }

  get createDate(): Date {
    return this._createDate;
  }

  set createDate(value: Date) {
    this._createDate = value;
  }

  get updateDate(): Date {
    return this._updateDate;
  }

  set updateDate(value: Date) {
    this._updateDate = value;
  }

  get active(): boolean {
    return this._active;
  }

  set active(value: boolean) {
    this._active = value;
  }

  get notificationServices(): NotificationService[] {
    return this._notificationServices;
  }

  set notificationServices(value: NotificationService[]) {
    this._notificationServices = value;
  }

  private _email: string;
  private _password: string;
  private _createDate: Date;
  private _updateDate: Date;
  private _active: boolean;
  private _notificationServices: NotificationService[];

}
