import {Notification} from './notification';
import {ParseOptions} from './parse-options';

export class Event {
  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get documents(): string[] {
    return this._documents;
  }

  set documents(value: string[]) {
    this._documents = value;
  }

  get place(): string {
    return this._place;
  }

  set place(value: string) {
    this._place = value;
  }

  get criticality(): number {
    return this._criticality;
  }

  set criticality(value: number) {
    this._criticality = value;
  }

  get tags(): string[] {
    return this._tags;
  }

  set tags(value: string[]) {
    this._tags = value;
  }

  get startTime():Date {
    return this._startTime;
  }

  set startTime(value:Date) {
    this._startTime = value;
  }

  get endTime(): Date {
    return this._endTime;
  }

  set endTime(value: Date) {
    this._endTime = value;
  }

  get organizer(): string {
    return this._organizer;
  }

  set organizer(value: string) {
    this._organizer = value;
  }

  get notifications(): Notification[] {
    return this._notifications;
  }

  set notifications(value: Notification[]) {
    this._notifications = value;
  }

  get parseOptions(): ParseOptions {
    return this._parseOptions;
  }

  set parseOptions(value: ParseOptions) {
    this._parseOptions = value;
  }

  private _name: string;
  private _documents: string[];
  private _place: string;
  private _criticality: number;
  private _tags: string[];
  private _startTime: Date;
  private _endTime: Date;
  private _organizer: string;
  private _notifications: Notification[];
  private _parseOptions: ParseOptions;

}
