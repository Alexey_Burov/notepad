import {NotificationService} from './notification-service';

export enum TimeTipe{
  BEFORE_TIME,
  IN_TIME
}


export class Notification {
  get type(): TimeTipe {
    return this._type;
  }

  set type(value: TimeTipe) {
    this._type = value;
  }
  get service(): NotificationService {
    return this._service;
  }

  set service(value: NotificationService) {
    this._service = value;
  }

  get message(): string {
    return this._message;
  }

  set message(value: string) {
    this._message = value;
  }

  private _type: TimeTipe;
  private _service: NotificationService;
  private _message: string;
}
