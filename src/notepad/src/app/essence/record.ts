export class Record {
  // private _type: enum {TEXT, PAINTING};
  get text(): string {
    return this._text;
  }

  set text(value: string) {
    this._text = value;
  }

  get createDate(): Date {
    return this._createDate;
  }

  set createDate(value: Date) {
    this._createDate = value;
  }

  get updateDate(): Date {
    return this._updateDate;
  }

  set updateDate(value: Date) {
    this._updateDate = value;
  }

  get color(): number {
    return this._color;
  }

  set color(value: number) {
    this._color = value;
  }

  private _text: string;
  private _createDate: Date;
  private _updateDate: Date;
  private _color: number;
}
