import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CKEditorPluginComponent } from './ckeditor-plugin.component';

describe('CKEditorPluginComponent', () => {
  let component: CKEditorPluginComponent;
  let fixture: ComponentFixture<CKEditorPluginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CKEditorPluginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CKEditorPluginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
