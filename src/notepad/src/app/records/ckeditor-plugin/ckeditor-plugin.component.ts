import {Component} from '@angular/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-ckeditor-plugin',
  templateUrl: './ckeditor-plugin.component.html',
  styleUrls: ['./ckeditor-plugin.component.css']
})
export class CKEditorPluginComponent {

  Editor = ClassicEditor;
  constructor() {
  }

}
