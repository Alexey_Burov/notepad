import { Injectable } from '@angular/core';
import {Record} from '../../essence/record';

@Injectable({
  providedIn: 'root'
})
export class RecordService extends Record {
  constructor() {
    super();
  }
}
