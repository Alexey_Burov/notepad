import { Injectable } from '@angular/core';
import {Notification} from '../essence/notification';

@Injectable({
  providedIn: 'root'
})
export class NotificationService extends Notification {
  constructor() {
    super();
  }
}
