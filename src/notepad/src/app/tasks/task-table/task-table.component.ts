import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {TaskService} from '../services/task.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {EstimationType, StateType} from '../../essence/task';
import {Notification} from '../../essence/notification';
import {MatDialog} from '@angular/material/dialog';
import {PopupEditTaskComponent} from '../popup-edit-task/popup-edit-task.component';
import {PopupEditEventComponent} from '../../events/popup-edit-event/popup-edit-event.component';

export interface TaskData {
  id: string;
  name: string;
  description: string;
  checkList: string[];
  documents: string[];
  estimation: number;
  estimationType: EstimationType;
  dedline: Date;
  criticality: (0 | 1 | 2);
  tags: string[];
  startTime: Date;
  endTime: Date;
  state: StateType;
  notifications: Notification[];
}

@Component({
  selector: 'app-task-table',
  templateUrl: './task-table.component.html',
  styleUrls: ['./task-table.component.css']
})
export class TaskTableComponent implements OnInit {

  dataSource: MatTableDataSource<TaskData>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private taskService: TaskService,
    public dialog: MatDialog
  ) {

    const tasks =  Array.from({length: taskService.taskList.length}, (_, k) => taskService.taskList[k].toObject());

    console.log(tasks);
    this.dataSource = new MatTableDataSource(tasks);
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openDialog(name: string) {
     const dialogRef = this.dialog.open(PopupEditTaskComponent, {
      width: '250px',
      data: {taskName: name}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
    });
  }
}
