import {TestBed} from '@angular/core/testing';

import {TaskService} from './task.service';
import {CriticalityType, EstimationType, StateType, Task} from '../../essence/task';

describe('TaskService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TaskService = TestBed.get(TaskService);
    expect(service).toBeTruthy();
  });

  it('addTask test', () => {
    const service: TaskService = TestBed.get(TaskService);
    const task = new Task();
    task.checkList = [];
    task.criticality = CriticalityType.NOTIMPORTANT;
    task.dedline = new Date(2020, 0, 1);
    task.name = 'test task';
    service.addTask(
      'test task',
      'qwe',
      [],
      [],
      1,
      EstimationType.DAYS,
      new Date(0, 0, 0),
      1,
      [],
      new Date(0, 0, 0),
      new Date(0, 0, 0),
      StateType.DONE,
      []
      );
    expect('test task').toEqual(service.taskList[0].name);
  });

  it('removeTaskByName', () => {
    const service: TaskService = TestBed.get(TaskService);
    for (let i = 0; i < 10; i++) {
      service.addTask('test task ' + i,
        'qwe',
        [],
        [],
        1,
        EstimationType.DAYS,
        new Date(0, 0, 0),
        1,
        [],
        new Date(0, 0, 0),
        new Date(0, 0, 0),
        StateType.DONE,
        []);
    }
    service.removeTaskByName('test task 0');
    console.log(service.taskList);

    expect(9).toEqual(service.removeTaskByName('test task 0').length);
  });
});
