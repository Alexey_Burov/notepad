import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UploadFileComponent} from './upload-file.component';
import {DragDropDirective} from './drag-drop.directive';

@NgModule({
  declarations: [
    UploadFileComponent,
    DragDropDirective,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    UploadFileComponent,
    DragDropDirective,
  ]
})
export class UploadFileModule { }
