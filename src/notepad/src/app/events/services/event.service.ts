import { Injectable } from '@angular/core';
import {Event} from '../../essence/event';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  eventList:  Event[];

  constructor() {

  }
}
