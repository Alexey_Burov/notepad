import { TestBed } from '@angular/core/testing';

import { ParseOptionsService } from './parse-options.service';

describe('ParseOptionsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ParseOptionsService = TestBed.get(ParseOptionsService);
    expect(service).toBeTruthy();
  });
});
