import {Component, Inject, OnInit} from '@angular/core';
import {EventService} from '../services/event.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup} from '@angular/forms';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-popup-edit-event',
  templateUrl: './popup-edit-event.component.html',
  styleUrls: ['./popup-edit-event.component.css']
})
export class PopupEditEventComponent implements OnInit {

  // constructor(
  //   private taskService: EventService
  // ) { }


  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    // this.dialogRef.close();
  }

  ngOnInit() {
  }

}
